import p5 from "p5"

export class Player {
  p: p5
  position: p5.Vector
  velocity: p5.Vector
  acceleration: p5.Vector

  constructor(x: number, y: number, p: p5) {
    this.p = p
    this.position = p.createVector(x, y)
    this.velocity = p.createVector(0, 0)
    this.acceleration = p.createVector(0, 0)
  }

  public canvasBoundary() {
    if (this.position.y >= this.p.height) {
      this.position.y = this.p.height - 15
      this.velocity.y *= -1
    } else if (this.position.x >= this.p.width) {
      this.position.x = this.p.width - 15
      this.velocity.x *= -1
    } else if (this.position.x <= 0) {
      this.position.x = 0
      this.velocity.x *= -1
    }
  }

  addPull(vector: p5.Vector) {
    this.acceleration.add(vector)
  }

  thrust(direction: string) {
    if (direction === "left") {
      let thrustPull = this.p.createVector(1, 0)
      this.acceleration.add(thrustPull)
    } else {
      this.acceleration.set(0, 0)
    }
  }

  public update() {
    this.velocity.add(this.acceleration)
    this.position.add(this.velocity)
    this.acceleration.set(0, 0)
  }
  public render() {
    this.p.ellipse(this.position.x, this.position.y, 30)
  }
}
