import p5 from 'p5'
import { KeyboardController } from './core/keyboardController'
import { Player } from './objects/Player'

const createContainerEl = (type: string, className?: string) => {
    const el = document.createElement(type)
    el.classList.add(className ?? 'container')
    document.body.appendChild(el)
    return el
}

const container = createContainerEl('div')
let keyboardController = new KeyboardController()

const sketch = (p: p5) => {
    let player: Player

    p.setup = () => {
        p.createCanvas(500, 500)
        player = new Player(p.width / 2, 50, p)
        
    }

    p.draw = () => {
        p.background('#ccc')
        if (keyboardController.isKeyPressed('KeyA')) {
            player.thrust('left')
        } else {
            player.acceleration.set(0, 0)
        }
        
        let gravity = p.createVector(0, 0.3)
        player.addPull(gravity)
        player.update()
        player.canvasBoundary()
        player.render()
    }
}

new p5(sketch, container)